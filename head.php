<!DOCTYPE html>
<html lang="fr" dir="ltr">
<head>
    <meta charset="utf-8">
    <title>Cv numérique</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="icon" href="/assets/img/logoNana.png">
    <link rel="stylesheet" type="text/css" media="screen" href="/assets/css/newMain.css">
    <script src="https://kit.fontawesome.com/2fd3b685b5.js" crossorigin="anonymous"></script>
</head>
<body class="container text-center myBody">
        <nav>
            <a href="index.php"><h1 class="m-2">Developpeuse Web Junior</h1></a>
            <div class="decoText"></div>
            <h2 class="fs-1">Vera Dos Santos</h2>
        </nav>
<!-- Button trigger modal -->
<button type="button" class="btnContact" data-bs-toggle="modal" data-bs-target="#exampleModal">Contacts</button>

<!-- Modal -->
<div class="modal fade " id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog myModal">
    <div class="modal-content myModal">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Contacts : </h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <p><a href="tel:+33625934881"><i class="fas fa-mobile-alt"></i> 06-25-93-48-81</a></p>
        <p><a href="mailto:vera78ds@gmail.com"><i class="fas fa-at"></i> vera78ds@gmail.com</a></p>
        <p><a href="https://www.google.fr/maps/place/78370+Plaisir/@48.7794244,2.0969164,10.67z/data=!4m6!3m5!1s0x47e684e3a0c1b059:0xf26e18b145fd068a!8m2!3d48.817223!4d1.949075!16zL20vMDc2aHN6"> <i class="fas fa-home"></i> 78370 Plaisir</a></p>
      </div>
    </div>
  </div>
</div>
