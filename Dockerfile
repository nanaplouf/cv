FROM php:8.2-fpm-alpine

# Install pdo_mysql
#RUN docker-php-ext-install pdo pdo_mysql
# PHP-FPM defaults
ENV PHP_FPM_PM="dynamic"
ENV PHP_FPM_MAX_CHILDREN="10"
ENV PHP_FPM_START_SERVERS="4"
ENV PHP_FPM_MIN_SPARE_SERVERS="2"
ENV PHP_FPM_MAX_SPARE_SERVERS="4"
ENV PHP_FPM_MAX_REQUESTS="1000"

# Copy the PHP-FPM configuration file
COPY ./www.conf /usr/local/etc/php-fpm.d/www.conf

# Install composer
#COPY --from=composer:latest /usr/bin/composer /usr/local/bin/composer

# Copy the PHP application file
COPY . /var/www/public
WORKDIR /var/www/public
RUN cd /var/www/public
#RUN composer install --prefer-dist --no-dev
RUN chown -R www-data:www-data /var/www/public
