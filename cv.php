<?php include 'head.php' ?>
<div class="container">
    <div class="row">
        <div class="col-md-3 ms-auto m-3 p-2">
            <img src="assets/img/logoNana.png" alt="Logo de nanaplouf" width="100%">
        </div>
        <div class="col-md-7 ms-auto">
            <h3>Présentation</h3>
            <p class="fs-5 ms-5">De tempérament dynamique, curieux et passionné, je me suis lancée depuis
                plus de deux ans dans un projet personnel de création d'une d'application
                web et mobile. Pour cela, je me suis autoformée, j'ai suivi de nombreux
                cours en ligne. Compte tenu des résultats, cette activité est aussi devenue
                une passion quotidienne. J'ai concrétisé ce projet en intégrant une
                formation certifiante à La Manu. Apres l'optention du titre j'ai poursuivie ce
                projet en intégrant la formation CDA en alternance chez Magellan
                Consulting. Aujourd'hui je cherche une entreprise en tant que développeuse
                junior.
            </p>
        </div>
    </div>
    <div class="row justify-content-around mt-5">
        <a class="col-md-4 col-sm-3 btnCv m-5" id="formation">Diplômes</a>
    </div>
    <div class="row">
        <div class="col m-3">
            <h4>Concepteur Développeur d'Applications BAC +3</h4>
            <h5><i class="fa-solid fa-calendar-days m-2"></i> 2022-2023 <i class="fa-sharp fa-solid fa-location-dot m-2"></i> Manu campus de Versailles</h5>
            <ul class="text-start">
                <li>Conception: identifier, hiérarchiser et répondre aux besoins client.</li>
                <li>Gestion de projet: mise en place des bonnes pratiques en matière de gestion de projet, organiser le travail en équipe.</li>
                <li>Développement d’une API: développement d’une API permettant de centraliser les données qui seront accessibles par les différentes applications.</li>
                <li>Développement des couches: web, mobile, desktop.</li>
            </ul>
        </div>
        <div class="col m-3">
            <h4>Executive Program Développement web BAC +2</h4>
            <h5><i class="fa-solid fa-calendar-days m-2"></i> 2021-2022 <i class="fa-sharp fa-solid fa-location-dot m-2"></i> Manu campus de Versailles</h5>
            <ul class="text-start">
                <li>Savoir utiliser les technologies liées au développement web.</li>
                <li>Être capable de développer un site web avec une vision UX design.</li>
                <li>Savoir implémenter et exploiter une base de données. Savoir développer les composants d'accès aux données.</li>
                <li>Maîtriser les techniques de référencement d'un site web.</li>
                <li>Comprendre les enjeux et mettre en œuvre le développement adaptatif (Responsive) : mobile, tablette, ordinateur et TV.</li>
                <li>Développer une application web ou web mobile en intégrant les recommandations et bonnes pratiques de la cybersécurité.</li>
                <li>Savoir utiliser les méthodes agiles dans la gestion de projet.</li>
            </ul>
        </div>
    </div>
    <div class="row mt-4">
        <div class="col m-3">
            <h4>BAC PRO Hôtellerie Restauration - Salle</h4>
            <h5><i class="fa-solid fa-calendar-days m-2"></i> 2012-2010 <i class="fa-sharp fa-solid fa-location-dot m-2"></i> Alternance - Brasserie du théâtre à Versailles</h5>
            <ul class="text-start">
                <li>Travaux polyvalents qui ont permis de confirmer mes capacités d'adaptation, de résistance au
                stress, mon dynamisme et ma sociabilité.</li>
            </ul>
        </div>
        <div class="col m-3">
            <h4>CAP Hôtellerie Restauration - Salle</h4>
            <h5><i class="fa-solid fa-calendar-days m-2"></i> 2010-2008 <i class="fa-sharp fa-solid fa-location-dot m-2"></i> Alternance - Taverne de Maître Kanter</h5>
            <ul class="text-start">
                <li>Formation adaptée à une clientèle exigeante, en recherche de qualité et de prestations avancées.
                Cela m'a permis d'accroître ma créativité et mon esprit de rigueur.</li>
            </ul>
        </div>
    <div class="row mt-4">
        <div class="col m-3">
            <h4>Formation API Restauration</h4>
            <h5><i class="fa-solid fa-calendar-days m-2"></i>2018 <i class="fa-sharp fa-solid fa-location-dot m-2"></i> Infres - Parcours du chef</h5>
            <ul class="text-start">
                <li>Approfondir les connaissances du personnel sur les bases du « management de nos collaborateurs » et partager les expériences d’encadrement pour mieux se situer.</li>
                <li>Permettre le développement personnel : maîtrise de soi, accroître son influence, augmenter son charisme.</li>
                <li>Amélioration des performances personnelles, collectives et l’esprit d’équipe.</li>
                <li>Améliorer la gestion du budget défini pour le bon fonctionnement d’une exploitation</li>
                <li>adapter et d’uniformiser les méthodes et les documents pour une meilleure gestion.</li>
            </ul>
        </div>
    </div>
    </div>
    <div class="row justify-content-around mt-5">
        <a class="col-md-4 col-sm-3 btnCv m-5" id="experience">Expériences</a>
    </div>
    <div class="row">
    <div class="col m-3">
        <h4>Apprentie développeuse</h4>
        <h5><i class="fa-solid fa-calendar-days m-2"></i>depuis avril 2022 à aujourd'hui <i class="fa-sharp fa-solid fa-location-dot m-2"></i> Magellan Consulting - Newbound</h5>
        <p class="text-start">
        Campagne marketing sur CRM Selligent marketing cloud. 
        Maintien en conditions opérationnelles, développement et évolution des sites web des clients de la société :
        Angular, node js, Symfony, gestion de projet, gitLab, prise en charge des
        tickets clients sur Mantis.
        Rédaction de documentions sur les différents  projets.
        Livraisons en pre-prod puis en prod, sur les serveurs Linux des clients</p>
    </div>
    <div class="row">
    <div class="col m-3">
        <h4>Manager restaurant</h4>
        <h5><i class="fa-solid fa-calendar-days m-2"></i>2018-2022 <i class="fa-sharp fa-solid fa-location-dot m-2"></i> Api Restauration \ Paris - Orange Arcueil</h5>
        <p class="text-start">Management, relation client, B2B, Comptabilité, paie, gestion du budget, des commandes, organisation, procédures, démarches administratives, gestion des risques et des incidents, aspects sanitaires.</p>
    </div>
    <div class="col m-3">
        <h4>Assistante manager</h4>
        <h5><i class="fa-solid fa-calendar-days m-2"></i>2017-2018 <i class="fa-sharp fa-solid fa-location-dot m-2"></i> Api Restauration \ Paris - Orange Garden</h5>
        <p class="text-start">Management par intérim, préparation des commandes, paie, suivi de la facturation, gestion de proximité, travaux polvalents et adaptation suivant les besoins et situations.</p>
    </div>
    <div class="row justify-content-around mt-5">
        <h4>Compétences :</h4>
    </div>
    <div class="row justify-content-around mt-5">
        <img class="col-sm-1 m-2" src="assets/img/html.png" alt="logo HTML 5">
        <img class="col-sm-1 m-2" src="assets/img/css.png" alt="logo CSS 3">
        <img class="col-sm-1 m-2" src="assets/img/js.png" alt="logo javascript">
        <img class="col-sm-1 m-2" src="assets/img/php.png" alt="logo php">
        <img class="col-sm-1 m-2" src="assets/img/symfony.png" alt="logo symfony">
        <img class="col-sm-1 m-2" src="assets/img/wp.png" alt="logo Wordpress">
        <img class="col-sm-1 m-2" src="assets/img/react.png" alt="logo React">
        <img class="col-sm-1 m-2" src="assets/img/ts.png" alt="logo Ts">
        <img class="col-sm-1 m-2" src="assets/img/angular.png" alt="logo Angular">
        <img class="col-sm-1 m-2" src="assets/img/node.png" alt="logo Node">
        <img class="col-sm-1 m-2" src="assets/img/npm.png" alt="logo npm">
        <img class="col-sm-1 m-2" src="assets/img/sql.png" alt="logo MySQL">
        <img class="col-sm-1 m-2" src="assets/img/wamp.png" alt="logo WAMP">
        <img class="col-sm-1 m-2" src="assets/img/mui.png" alt="logo Mui">
        <img class="col-sm-1 m-2" src="assets/img/bootstrap.png" alt="logo bootstrap">
        <img class="col-sm-1 m-2" src="assets/img/github.png" alt="logo github">
        <img class="col-sm-1 m-2" src="assets/img/gitlab2.png" alt="logo gitlab">
        <img class="col-sm-1 m-2" src="assets/img/vsc.png" alt="logo Visual studio code">
        <img class="col-sm-1 m-2" src="assets/img/figma.png" alt="logo figma">
        <img class="col-sm-1 m-2" src="assets/img/photo.jpg" alt="logo ps">
    </div>
    <div class="decoText mt-5"></div>
    <div class="row justify-content-around mt-5">
        <h4>Langues :</h4>
        <img class="col-sm-1 m-2" src="assets/img/portugal.png" alt="drapeau du Portugal">
        <img class="col-sm-1 m-2" src="assets/img/anglais.png" alt="drapeau d'Espagne">
        <img class="col-sm-1 m-2" src="assets/img/espagne.png" alt="drapeau d'Angleterre">
    </div>
    <div class="decoText mt-5"></div>
    <div class="row mt-5">
    <h4 class="mt-5">Compétences professionnelles :</h4>
    <p class="col fs-5">Management d'équipe</p>
    <p class="col fs-5">Gestion de projets</p>
    <p class="col fs-5">Gestion financière</p>
    <p class="col fs-5">Autonomie</p>
    <p class="col fs-5">Travail en équipe</p>
    </div>
    <div class="decoText mt-5"></div>
    <div class="row justify-content-around mt-5">
        <a class="col-md-4 col-sm-3 btnCv m-5" id="link">Liens</a>
    </div>
    <div class="col">
                <a href="https://github.com/dsvera" class="fs-5"><i class="fab fa-github lien"></i> Github</a>
            </div>
            <div class="col">
                <a href="https://gitlab.com/nanaplouf" class="fs-5"><i class="fab fa-gitlab lien"></i>  Gitlab</a>
            </div>
            <div class="col">
                <a href="https://www.linkedin.com/in/dossantosvera/" class="fs-5"><i class="fab fa-linkedin lien"></i>Linkedin</a>
            </div>
    <div class="row justify-content-around mt-5">
        <h4 class="mt-5">Centres d'intérêt</h4>
    </div>
    <div class="row">
            <p class="fs-5">Administration d'un groupe Facebook (+1700 adhérents) </p>
            <p class="fs-5">Photographie - Travaux créatifs et artistiques - Jeux coopératifs - Sport ( running, canicross)</p>
    </div>
    <div class="decoText mt-5"></div>
</div>
<?php include 'footer.php' ?>