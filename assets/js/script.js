fetch("../assets/js/portfolio.json")
  .then(response => response.json())
  .then(response => display(response.projet))
  function display(projet) {
      let container = document.createElement("div")
      container.className = "container row justify-content-around"
    //Tableau JSON:
    for (i = 0; i < projet.length; i++) {
        console.log(projet)
        let cardDiv = document.createElement("div")
        cardDiv.className = "card col-md-4 col-sm-12 m-4 cardPortefolio"
        cardDiv.style.width = "18rem"

        let cardImg = document.createElement("img")
        cardImg.className = "card-img-top"
        cardImg.src = projet[i].picture
        cardImg.alt = projet[i].alt

        let cardBodyDiv = document.createElement("div")
        cardBodyDiv.className = "card-body"

        let cardTitle = document.createElement("h5")
        cardTitle.className = "card-title"
        cardTitle.innerHTML = projet[i].title

        let cardP = document.createElement("p")
        cardP.className = "card-text"
        cardP.innerHTML = projet[i].language

        let cardA = document.createElement("a")
        cardA.className = "btn btnCard"
        cardA.innerText = "Voir le site"
        cardA.href = projet[i].link
        container.append(cardDiv)
        cardDiv.append(cardImg, cardBodyDiv)
        cardBodyDiv.append(cardTitle, cardP, cardA)
       // append des élements
    }
    portfolio.append(container)
  }